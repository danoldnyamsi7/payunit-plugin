let payunitContainer: HTMLDivElement = document.querySelector(
  ".payunit-button-container"
) as HTMLDivElement;
let isWindow: any = ''; let url:string = ''


let css =`.payunit-button-container{width: 100%;}.payunit-button-container button{width: 320px;padding-top: 0.3rem;cursor: pointer;max-width: 100%;border: none;margin-top: 1rem;background-color: #14a49c;border-radius: 5px;}.payunit-button-container svg{height: 40px;}.payunit-button-container .shadow-container {position: fixed;z-index: 10;top: 0;left: 0;width: 100%;height: 100vh;background-color: #000000f2;}.payunit-button-container .semi-container {width: 500px;height: 100%;display: flex;align-items: center;justify-content: center;flex-direction: column;max-width: 100%;margin: 0 auto;
color: white;}.payunit-button-container a {margin: 1rem 0;color: #14a49c;}.payunit-button-container button.close {background-color: inherit;font-size: 10px;color: white;position: absolute;right: 10px;top: 5px;width: 40px;}.payunit-button-container .svg {height: 25px;}.payunit-button-container .none {display: none;}
`
let html = `<button id="payunit-click"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 339.57 153.47"><defs><style>.cls-1{fill: #fff;}.cls-2{     fill: #f49405;}.cls-3 {fill: #00a59c;}</style></defs><g id="Layer_2" data-name="Layer 2"><g id="Calque_1" data-name="Calque 1"><path class="cls-1"
d="M339.57,20V121.88a20,20,0,0,1-20,20H56.27a57.67,57.67,0,0,1-8.89-3.57c-11.12-5.57-19.42-14-23.25-21.54C15.54,99.88,29.79,76.33,36.63,65c.19-.29.36-.58.54-.86L42.83,81.5,71.58,22.63,9.36,43.12l19.08,9.27q-1.86,2.7-3.56,5.51c-4.74,7.81-11.2,18-14.58,30.32-2.55,9.3-2.22,15.5-2.61,15.48-.62,0-1-15.68,0-83.69a20,20,0,0,1,20-20H319.56A20,20,0,0,1,339.57,20Z" /><path class="cls-2"d="M83.88,146.49c-31.61,14.18-64.54,5.6-77.58-15.3-10-16.13-5.43-35.06-3.92-40.42-.31,7.07.13,22,9.75,35.38C26.43,146.09,55.09,154.44,83.88,146.49Z" /><path class="cls-3"d="M110.72,67c0,11.08-7.69,17.29-20,17.29H75.81l-4.53,13.6h-4.5L79.89,54.77H95.34C105.06,54.77,110.72,59.26,110.72,67Zm-4.55.43c0-5.66-4-8.67-11.2-8.67H83.64L77,80.24H90.78C100.57,80.24,106.17,75.87,106.17,67.38Z" />
<path class="cls-3"d="M149,66.29l-6.35,31.84H138.5l1-5a16.32,16.32,0,0,1-12.41,5.33c-8.17,0-13.86-5.09-13.86-13.5,0-10.89,7.63-18.88,18-18.88,6,0,10.29,2.48,12.1,6.9l1.34-6.66Zm-7.14,13.44c0-6.24-3.81-9.93-10.47-9.93-7.93,0-13.8,6.29-13.8,15,0,6.23,3.81,9.87,10.47,9.87C136,94.62,141.83,88.38,141.83,79.73Z" /><path class="cls-3" d="M187.71,66.29l-23.24,35.95c-3.94,6.05-6.9,7.93-11.5,7.93-3,0-6-1-7.57-2.84l2.3-3.15a7.43,7.43,0,0,0,5.63,2.3c2.73,0,4.84-1.27,7.32-5.15l2.24-3.39L155,66.29h4.35l6.6,27.11,17.37-27.11Z" /><path class="cls-2" d="M223.51,66.05V98.13H218V93.28a13.06,13.06,0,0,1-10.89,5.21c-8.3,0-13.8-4.54-13.8-14V66.05h5.81V83.84c0,6.3,3.14,9.44,8.65,9.44,6,0,9.93-3.75,9.93-10.65V66.05Z" /><path class="cls-2"d="M265,79.66V98.13h-5.81V80.33c0-6.29-3.15-9.38-8.65-9.38-6.18,0-10.17,3.69-10.17,10.65V98.13h-5.81V66.05h5.57v4.84c2.36-3.27,6.47-5.15,11.56-5.15C259.52,65.74,265,70.22,265,79.66Z" /><path class="cls-2"d="M274.78,56.12a3.82,3.82,0,0,1,3.93-3.81,3.78,3.78,0,1,1-3.93,3.81Zm1,9.93h5.81V98.13h-5.81Z" /><path class="cls-2"d="M310.73,96.25a11,11,0,0,1-7,2.24c-6.48,0-10.17-3.57-10.17-10V70.83h-5.45V66.05h5.45V59h5.81v7h9.2v4.78h-9.2V88.2c0,3.45,1.82,5.39,5,5.39a7.3,7.3,0,0,0,4.54-1.52Z" /></g></g>
</svg></button><div class="shadow-container none"><div class="semi-container"><button class="close" onclick="closeOverlay()"><svg xmlns="http://www.w3.org/2000/svg" class="svg" fill="none" viewBox="0 0 24 24" stroke="currentColor"> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
</svg></button> <svg id="svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 339.57 153.47"> <defs><style>.cls-1 {fill: #fff;}.cls-2 { fill: #f49405;}
.cls-3 {fill: #00a59c;}</style></defs><g id="Layer_2" data-name="Layer 2"><g id="Calque_1" data-name="Calque 1"><path class="cls-1" d="M339.57,20V121.88a20,20,0,0,1-20,20H56.27a57.67,57.67,0,0,1-8.89-3.57c-11.12-5.57-19.42-14-23.25-21.54C15.54,99.88,29.79,76.33,36.63,65c.19-.29.36-.58.54-.86L42.83,81.5,71.58,22.63,9.36,43.12l19.08,9.27q-1.86,2.7-3.56,5.51c-4.74,7.81-11.2,18-14.58,30.32-2.55,9.3-2.22,15.5-2.61,15.48-.62,0-1-15.68,0-83.69a20,20,0,0,1,20-20H319.56A20,20,0,0,1,339.57,20Z" /><path class="cls-2" d="M83.88,146.49c-31.61,14.18-64.54,5.6-77.58-15.3-10-16.13-5.43-35.06-3.92-40.42-.31,7.07.13,22,9.75,35.38C26.43,146.09,55.09,154.44,83.88,146.49Z" /><path class="cls-3" d="M110.72,67c0,11.08-7.69,17.29-20,17.29H75.81l-4.53,13.6h-4.5L79.89,54.77H95.34C105.06,54.77,110.72,59.26,110.72,67Zm-4.55.43c0-5.66-4-8.67-11.2-8.67H83.64L77,80.24H90.78C100.57,80.24,106.17,75.87,106.17,67.38Z" />
<path class="cls-3"d="M149,66.29l-6.35,31.84H138.5l1-5a16.32,16.32,0,0,1-12.41,5.33c-8.17,0-13.86-5.09-13.86-13.5,0-10.89,7.63-18.88,18-18.88,6,0,10.29,2.48,12.1,6.9l1.34-6.66Zm-7.14,13.44c0-6.24-3.81-9.93-10.47-9.93-7.93,0-13.8,6.29-13.8,15,0,6.23,3.81,9.87,10.47,9.87C136,94.62,141.83,88.38,141.83,79.73Z" /><path class="cls-3"d="M187.71,66.29l-23.24,35.95c-3.94,6.05-6.9,7.93-11.5,7.93-3,0-6-1-7.57-2.84l2.3-3.15a7.43,7.43,0,0,0,5.63,2.3c2.73,0,4.84-1.27,7.32-5.15l2.24-3.39L155,66.29h4.35l6.6,27.11,17.37-27.11Z" /><path class="cls-2"d="M223.51,66.05V98.13H218V93.28a13.06,13.06,0,0,1-10.89,5.21c-8.3,0-13.8-4.54-13.8-14V66.05h5.81V83.84c0,6.3,3.14,9.44,8.65,9.44,6,0,9.93-3.75,9.93-10.65V66.05Z" /><path class="cls-2"d="M265,79.66V98.13h-5.81V80.33c0-6.29-3.15-9.38-8.65-9.38-6.18,0-10.17,3.69-10.17,10.65V98.13h-5.81V66.05h5.57v4.84c2.36-3.27,6.47-5.15,11.56-5.15C259.52,65.74,265,70.22,265,79.66Z" /><path class="cls-2"d="M274.78,56.12a3.82,3.82,0,0,1,3.93-3.81,3.78,3.78,0,1,1-3.93,3.81Zm1,9.93h5.81V98.13h-5.81Z" /><path class="cls-2"d="M310.73,96.25a11,11,0,0,1-7,2.24c-6.48,0-10.17-3.57-10.17-10V70.83h-5.45V66.05h5.45V59h5.81v7h9.2v4.78h-9.2V88.2c0,3.45,1.82,5.39,5,5.39a7.3,7.3,0,0,0,4.54-1.52Z" /></g></g>
</svg><p>Don’t see the secure Payunit browser? We’ll help you re-launch the window to complete your purchase</p> <a class="click-to-continue" style="cursor:pointer; text-decoration:underline;">click to continue</a></div></div>`

const displayButton = () => {
    payunitContainer.innerHTML = html;
    let s = document.createElement('style')
    s.innerHTML = css
    document.querySelector('head')?.appendChild(s)
}

// server side rendering
/* 
1. get all script tags and check if the src attribute matches https://payunit.net/*****. use try catch for this.
2. get the when there is an existence of a script with that src and the needed credentials, popup the window and display the hosted page payment.
3. check for payment completion and notify both the payunit server and the client.  


*/

let getScript = () => {
  let scripts: any = document.querySelectorAll(
    "script"
  ) as unknown as HTMLElement;
  for (let i of scripts) {
    if (i.src) return i.src;
  }
  return;
};

// let checkForPayunitCredentials = (urlPayload: string | any) => {
//     const urlFormat = ''
//     let url = getScript();
//     if (!url)
//     throw new Error(
//       "Javascript SDK Not Found! Expected https://payunit.net/CREADENTIALS?MERCHANT_ID=********?MERCHANT_PASS=**********"
//     );
//   console.log(url);
// };

/* 
    host : https://moc-payunit.herokuapp.com/pay
    method : post
    header : { x-api-key : 3f9853d213e12220c60559a2b7ed9840eda22247}
    body : {
        "environment" : "test" ,
        "amount" : your_amount ,
        "api-user" : "payunit_sand_LNFwOICmv" ,
        "api-password" : "763a7903-1421-478e-91da-4bea7214636f"
    }
*/

// checkForPayunitCredentials("");

// NEW SCRIPT IMPLIMENTATION
// OPEN POPUP;
 let newWindow:any;
 const openWindow = (url:string) => {
    let width = screen.width < 570 ? screen.width : 570;
    let height = screen.height < 700 ? screen.height : screen.height - 260;
    let top = screen.width < 500 ? 0 : height / 5,   left = screen.width < 500 ? 0 : screen.width / 3;
    newWindow = window.open(url, 'payunit-container',  `height=${height},width=${width},left=${left},top=${top},resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes`)
 }

//   check if online
let checkIFOnline = () => {
    window.addEventListener(
      "online",
      (e) => {
        console.log("And we're back :).");
      },
      false
    );
    window.addEventListener(
      "offline",
      (e) => {
        console.log("Connection is down.");
      },
      false
    );
  };


//   check if script has been embeded in another app
const isScript = () => {
    let scripts: any = document.querySelectorAll(
        "script"
      ) as unknown as HTMLElement;

      if(!scripts) return null
      else{
        for (let i of scripts) {
          if (i.src.includes('/plugin/dist/main.js') && i.type.includes('text/javascript')) return i.src;
        }
        return null;
      }
}

// get information from the embeded text edidtor
const getFromStorage = () => {
    let data = localStorage.getItem('data')
}

let isScripts = isScript()

const urls = () => {
  if(!isScripts){
    throw new Error("plugin not found");
  }
  else{
    // payunit.button()
  }
}
let closeWindow = () => newWindow.close();

let showOverlay = () => {
  let overlayContainer = document.querySelector('.shadow-container') as unknown as HTMLDivElement
  overlayContainer.classList.remove('none')
}


let closeOverlay = () => {
  let overlayContainer = document.querySelector('.shadow-container') as unknown as HTMLDivElement
  overlayContainer.classList.add('none')
  if(!newWindow.closed){
    closeWindow()
  }
}

// payunit
class Payunit {
  button(data:{initialize:payload}) {
    let button = document.getElementById('payunit-click')
    button?.addEventListener('click', e => {
      
      
      url = 'https://moc-payunit.herokuapp.com/pay'
      console.log(data.initialize);
      let dataPayload:any = {
        "environment" : data.initialize.mode,
        "amount" : data.initialize.amount,
        "api-user" : data.initialize.api_user,
        "api-password" : data.initialize.api_password
      }   
      this.process(dataPayload, url, data.initialize.api_key).then(data => {
         showOverlay()
         openWindow(data['url-payment'])
         console.log(newWindow.opener);
       })
    })

  }

  initialize(data:Object){
    console.log(data);
  }

 async process(payload:string, uri:string = url, apiKey:string):Promise<any>{
   let headers = new Headers();
   headers.append("x-api-key",apiKey)
   headers.append("Content-Type","application/json")
  try {
    const requestOptions:RequestInit = {
      method: 'POST',
      headers,
      body: JSON.stringify(payload)
    };
    const response = await fetch(uri, requestOptions)
      return response.json()
  } catch (error) {
    throw  Error(error as unknown as string);
  }
  }

  get data(){
    return 
  }

  finishedPayment(url:string){
    let button = document.getElementById('payunit-click')
    button?.addEventListener('click', e => {
      e.preventDefault()
       showOverlay()
       openWindow(url)
    })
  }
}



let payunit = new Payunit()

const monitor = () => {
  
  setInterval(() => {
      if (newWindow  && newWindow.closed) { 
        let overlayContainer = document.querySelector('.shadow-container') as unknown as HTMLDivElement
        overlayContainer.classList.add('none')
      }
      
    }, 100);
}


const clickToContinue = () => {
  let a = document.querySelector('.click-to-continue') as unknown as HTMLDivElement
  a.addEventListener('click', e => {
    e.preventDefault()
    if(url !== ''){
      openWindow(url)
    }    
  })
}

// interface 
interface ServerResponse {
  environment:string,
  response_url:string
}

interface payload {
  mode : "test" | "live" ,
  amount : string ,
  api_user : string ,
  api_password : string,
  api_key:string
}

enum DefaultPayload {
  environment = "test",
  amount = "500",
  api_user = "payunit_sand_LNFwOICmv",
  api_password = "763a7903-1421-478e-91da-4bea7214636f",
  api_key = '3f9853d213e12220c60559a2b7ed9840eda22247'
}

// call all functions not called
checkIFOnline();
displayButton()
urls()
clickToContinue()
monitor()
